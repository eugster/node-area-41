import { Vrc } from 'vrc';
import * as fs from 'fs';

export interface Conf {
    file: string;
    indent: 'dots' | 'list';
}

export interface PageEntry {
    format: string;
    slug: string;
    title: string;
}

export interface ExtendedPageEntry extends PageEntry {
    level: number;
    isPage: boolean;
    parentSlug: string;
}

const conf = new Vrc<Conf>( 'kdenlivetoc', [
    { name: 'file', type: 'string', dflt: undefined, desc: 'Input JSON file' },
    { name: 'indent', type: 'string', dflt: 'dots', desc: 'How to indent', options: [ 'dots', 'list' ] },
], {
    description: 'Generates a new TOC for a GitLab wiki. ' +
        'Save the JSON from /api/v4/projects/<project-id>/wikis in a file to use this tool.\n\n' +
        'The project ID can be obtained from the project site on GitLab.'
} ).run();

const entryToMd = ( entry: ExtendedPageEntry ): string => {

    const name = entry.title[ 0 ].toUpperCase() + entry.title.substring( 1 );

    const prefix = conf.conf.indent === 'dots'
        ? '· '.repeat( entry.level - 1 )
        : '  '.repeat( entry.level - 1 ) + '* ';

    const suffix = conf.conf.indent === 'dots'
        ? '  '
        : '';

    const item = entry.isPage ? `[${name}](${entry.slug})` : name;

    return `${prefix}${item}${suffix}`;
}
const getParentSlug = ( slug: string ): string => slug.split( '/' ).slice( 0, -1 ).join( '/' )
const getSlugLevel = ( slug: string ): number => slug.split( '/' ).length

const file = fs.readFileSync( conf.conf.file, { encoding: 'utf8' } );
const pages: PageEntry[] = JSON.parse( file );

const wikiPages = pages.map( ( el ) => Object.assign( {}, el, {
    level: getSlugLevel( el.slug ),
    parentSlug: getParentSlug( el.slug ),
    isPage: true,
} ) );

class WikiToc {
    constructor( entries: ExtendedPageEntry[] ) {
        for ( let entry of entries ) {
            if ( this.entries.length > 0 ) {

                const parentSlug = getParentSlug( entry.slug ).toLowerCase();
                const previousEntry = this.entries[ this.entries.length - 1 ];
                const prevSlug = previousEntry.slug.toLowerCase();
                const prevParentSlug = getParentSlug( previousEntry.slug ).toLowerCase();

                if ( parentSlug === '' || parentSlug === prevSlug || prevParentSlug.startsWith( parentSlug ) ) {
                    // Nothing to do
                } else {
                    this.entries.push( {
                        slug: parentSlug,
                        level: getSlugLevel( parentSlug ),
                        format: undefined,
                        isPage: false,
                        parentSlug: getParentSlug( parentSlug ),
                        title: parentSlug.split( '/' ).pop(),
                    } );
                }
            }
            this.entries.push( entry );
        }
    }

    entries: ExtendedPageEntry[] = [];
}

const sortedPages = wikiPages.sort( ( a, b ) => {
    if ( a.slug === 'home' ) {
        return -1;
    }
    if ( b.slug === 'home' ) {
        return 1;
    }
    return a.slug.localeCompare( b.slug );
} )


const toc = new WikiToc( sortedPages );
console.log( toc.entries.map( entryToMd ).join( '\n' ) )

